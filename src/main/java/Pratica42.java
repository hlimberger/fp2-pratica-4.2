/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import utfpr.ct.dainf.if62c.pratica.*;
/**
 *
 * @author Henrique Kolling Limberger
 */
public class Pratica42 {
    public static void main(String[] args){        Elipse el = new Elipse(4.2, 2.1);
        System.out.println("Elipse 4 x 2 - a área é " + el.getArea() + " e o perímetro é " + el.getPerimetro());
        Elipse el2 = new Elipse(5.7, 5.7);
        System.out.println("Elipse 4 x 4 - a área é " + el2.getArea() + " e o perímetro é " + el2.getPerimetro());
        Circulo cir = new Circulo(5.7);
        System.out.println("Circulo 5.7 x 5.7 - a área é " + cir.getArea() + " e o perímetro é " + cir.getPerimetro());
    }
}
